﻿
namespace _21IAS
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.memberTabControl = new System.Windows.Forms.TabControl();
            this.SuspendLayout();
            // 
            // memberTabControl
            // 
            this.memberTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memberTabControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.memberTabControl.Location = new System.Drawing.Point(0, 0);
            this.memberTabControl.Name = "memberTabControl";
            this.memberTabControl.SelectedIndex = 0;
            this.memberTabControl.Size = new System.Drawing.Size(800, 450);
            this.memberTabControl.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.memberTabControl);
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.Text = "21ИАС";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl memberTabControl;
    }
}

