﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Linq;

namespace _21IAS
{
    struct Question
    {
        public string Text { get; set; }
        public string[] Answers { get; set; }
        public int CorrectAnswer { get; set; }

        public Question(string text, string[] answers, int correctAnswer)
        {
            Text = text;
            Answers = answers;
            CorrectAnswer = correctAnswer;
        }
    }

    public partial class QuizForm : Form
    {
        // Метод для перемешки листа
        public static void Shuffle<T>(IList<T> list)
        {
            var rng = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        List<Question> questions;
        LinkedList<Panel> panels;
        LinkedListNode<Panel> currentPanel;
        bool[] check;

        public QuizForm(string QuizPath)
        {
            InitializeComponent();
            questions = new List<Question>();
            panels = new LinkedList<Panel>();

            /*
             *  Парсинг вопросов из файла 
            */
            var fs = new FileStream(QuizPath, FileMode.Open, FileAccess.Read);

            int count = 1;
            try
            {
                var buffer = "";
                var sr = new StreamReader(fs);

                var q = new Question();
                var text = new StringBuilder();
                var answers = new List<string>();

                while ((buffer = sr.ReadLine()) != null)
                {
                    switch (buffer[0])
                    {
                        case '*': // Вариант ответа
                            answers.Add(buffer.Substring(1).Trim());
                            break;
                        case '[': // Правильный ответ
                            q.Text = text.ToString();
                            q.Answers = answers.ToArray();
                            q.CorrectAnswer = int.Parse(
                                buffer.Substring(1, buffer.LastIndexOf(']') - 1));

                            if (answers.Count < q.CorrectAnswer)
                                throw new Exception("Номер правильного " +
                                    "ответа больше чем кол-во вопросов.");

                            text.Clear();
                            answers.Clear();

                            questions.Add(q);
                            q = new Question();

                            buffer = sr.ReadLine();
                            break;
                        default: // Текст вопроса
                            if ((buffer = buffer.Trim()) != "")
                                text.Append(buffer);
                            break;
                    }

                    count++;
                }
            }
            catch (Exception ex)
            {
                var lb = new Label();
                lb.Text = count + " : " + ex.Message;

                lb.Dock = DockStyle.Fill;
                lb.AutoSize = false;
                lb.TextAlign = ContentAlignment.MiddleCenter;
                lb.Font = new Font("Arial", 24, FontStyle.Bold);

                Controls.Add(lb);

                return;
            }
            finally
            {
                fs.Dispose();
            }

            Shuffle(questions);
            check = new bool[questions.Count];

            /*
             *  Заполнение формы 
            */
            for (int q = 0; q < questions.Count; q++)
            {
                int height = 0;

                var panel = new Panel();
                panel.Width = 500;
                panel.Location = new Point(0, 0);

                var question = new Label();
                question.Text = questions[q].Text;
                question.AutoSize = true;
                question.MaximumSize = new Size(panel.Width, 1000);
                question.Font = new Font("Arial", 12, FontStyle.Bold);
                question.Dock = DockStyle.Top;
                panel.Controls.Add(question);
                height += question.Height;

                var rbFont = new Font("Arial", 12);
                for (int a = 1; a <= questions[q].Answers.Length; a++)
                {
                    var rb = new RadioButton();
                    string[] answersArray = questions[q].Answers[a - 1].Split('@');
                    rb.Text = string.Join(Environment.NewLine, answersArray);
                    rb.Font = rbFont;
                    rb.Location = new Point(10, height);
                    rb.AutoSize = true;
                    rb.Height *= answersArray.Length;
                    height += rb.Height;
                    panel.Controls.Add(rb);

                    int temp = q;
                    if (a == questions[q].CorrectAnswer)
                        rb.CheckedChanged += (s, e) =>
                            check[temp] = rb.Checked;
                }

                var back = new Button();
                back.Text = "Назад";
                back.Font = rbFont;
                back.Location = new Point(40, height);
                back.Height = 30;
                panel.Controls.Add(back);

                var next = new Button();
                next.Text = "Далее";
                next.Font = rbFont;
                next.Location = new Point(120, height);
                next.Height = 30;
                height += next.Height;
                panel.Controls.Add(next);

                panel.Height = height;

                panels.AddLast(panel);

                if (currentPanel == null)
                {
                    currentPanel = panels.First;
                    MaximumSize = currentPanel.Value.Size + new Size(50, 50);
                    MinimumSize = currentPanel.Value.Size + new Size(50, 50);
                    Controls.Add(panel);
                }

                next.Click += (s, e) =>
                {
                    if (currentPanel.Next != null && currentPanel.Next.Value != null)
                    {
                        currentPanel = currentPanel.Next;
                        MaximumSize = currentPanel.Value.Size + new Size(50, 50);
                        MinimumSize = currentPanel.Value.Size + new Size(50, 50);
                        Controls.Clear();
                        Controls.Add(currentPanel.Value);
                    }
                    else
                    {
                        int countAns = 0;
                        foreach (var b in check)
                            countAns += b ? 1 : 0;

                        MessageBox.Show(countAns + " из " + check.Length, "Результат теста",
                            MessageBoxButtons.OK);
                        Close();
                    }
                };

                back.Click += (s, e) =>
                {
                    if (currentPanel.Previous != null && currentPanel.Previous.Value != null)
                    {
                        currentPanel = currentPanel.Previous;
                        MaximumSize = currentPanel.Value.Size + new Size(50, 50);
                        MinimumSize = currentPanel.Value.Size + new Size(50, 50);
                        Controls.Clear();
                        Controls.Add(currentPanel.Value);
                    }
                    else
                    {
                        MessageBox.Show("Вы на первом вопросе", "Сообщение",
                            MessageBoxButtons.OK);
                    }
                };
            }
        }
    }
}
